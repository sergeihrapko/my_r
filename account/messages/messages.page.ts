import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ToastrService} from 'ngx-toastr';
import {AccountService, PagerService, UserService} from '../../../service/index'
import {ConversationLight,ConversationFilterParam, User} from '../../../models/index'
@Component({
    selector: 'messages-page',
    templateUrl: 'messages.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class MessagesPage implements OnInit {
    private subscription: Subscription;
    private conversations: ConversationLight[];
    private filter: string = '';
    private filterParams: ConversationFilterParam[];
    private pager: any = {};
    private currentPage: number = 1;
    private preloader: boolean = true;
    private resultCounter: number = 0;
    private user: User = new User();

    constructor(private accountService: AccountService,
                private toastrService: ToastrService,
                private pagerService: PagerService,
                private userService: UserService) {
        this.subscription = this.userService.userChange.subscribe((data) => {
            this.user = data;
        });

    }


    ngOnInit() {
        this.user = this.userService.currentUser;
        this.getFilterParams();
        this.getConversation();
    }

    private getConversation() {
        this.preloader = true;
        if (!this.filter) {
            this.filter = 'active';
        }
        this.accountService.getConversations(this.filter, this.currentPage).subscribe((data)=> {
            this.conversations = data.json().results;
            this.resultCounter = data.json().count;
            this.preloader = false;
            this.setPage(this.currentPage, false);
            this.getFilterParams();
        });

    }

    private getFilterParams() {
        this.accountService.getConversationFilterParams().subscribe((data)=>{
            this.filterParams = data.json()
        })
    }

    private onChangeFilter(event: any) {
        this.currentPage = 1;
        this.getConversation()
    }

    public setPage(page: number, queried: boolean = true) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        this.pager = this.pagerService.getPager(this.resultCounter, page);
        if (queried) {
            this.currentPage = page;
            this.getConversation();
        }
    }

    private archiveConversation(conversation: ConversationLight) {
        conversation.archived_by.push(this.user.id);
        let data = {
            'archived_by': conversation.archived_by
        };
        this.accountService.addConversationMeta(conversation.id, data).subscribe(()=> {
            this.toastrService.success('Success!', 'Conversation archived');
            if(this.filter) {
                this.currentPage = 1;
            }
            this.getConversation();
        })

    }

    private starredConversation(conversation: ConversationLight, type: string = "add") {
        let successMessage: string = '';
        if (type == "remove") {
            let index = conversation.starred_by.indexOf(this.user.id);
            conversation.starred_by.splice(index, 1);
            successMessage = 'Conversation unstarred';

        }
        else {
            conversation.starred_by.push(this.user.id);
            successMessage = 'Conversation starred';
        }
        let data = {
            'starred_by': conversation.starred_by
        };

        this.accountService.addConversationMeta(conversation.id, data).subscribe((data) => {
            this.toastrService.success('Success!', successMessage);
            if(this.filter) {
                this.currentPage = 1;
            }
            this.getConversation();

        })
    }

    private readConversation(conversation:ConversationLight) {
        conversation.read_by.push(this.user.id);
        let data = {
            'read_by': conversation.read_by
        };
        this.accountService.addConversationMeta(conversation.id, data).subscribe(()=> {

        });
    }

    public getStatusMessage(item: any): string {
        if (item.read && item.replied && item.pending === false) {
            return 'Replied';
        }
        if (item.read && item.pending === false) {
            return 'Sent';
        }
        if (item.read) {
            return 'Read';
        }
        if (item.pending) {
            return 'Unread';
        }
    }
}
