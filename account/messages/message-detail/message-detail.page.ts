import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from "rxjs";
import {AdvertService, PagerService, ReviewsService, UserService, AccountService} from "../../../../service/index";
import {Advert, User, ConversationLight, Message} from '../../../../models/index';

@Component({
    selector: 'advert-detail',
    templateUrl: 'message-detail.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class MessageDetailPage implements OnInit {
    private subscription: Subscription;
    public conversationId: number;
    public conversation: ConversationLight;
    private user: User;
    private messages: Message[];
    private pager: any = {};
    private currentPage: number = 1;
    private preloader: boolean = true;
    private resultCounter: number = 0;
    private messageText: string;

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router,
                private toastrService: ToastrService,
                private userService: UserService,
                private pagerService: PagerService,
                private accountService: AccountService) {
        this.subscription = this.userService.userChange.subscribe((data) => {
            this.user = data;
        });
    }

    ngOnInit() {
        this.user = this.userService.currentUser;
        this.getConversationId();
        this.getConversation(this.conversationId);
    }

    public getConversation(id: number) {
        this.accountService.getConversation(id).subscribe((data)=> {
            this.conversation = data.json();
            this.getMessages()
        });

    }

    public getMessages() {
        this.preloader = true;
        this.accountService.readConversation(this.conversationId);
        this.accountService.getMessages(this.conversationId, this.currentPage).subscribe(
            (data) => {
                this.resultCounter = data.json().count;
                this.preloader = false;
                this.setPage(this.currentPage, false);
                this.messages = data.json().results;
            },
            (err) => {
                this.router.navigate(['/not_found']);
            }
        )
    }

    public setPage(page: number, queried: boolean = true) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        this.pager = this.pagerService.getPager(this.resultCounter, page);
        if (queried) {
            this.currentPage = page;
            this.getConversation(this.conversationId)
        }
    }

    public getConversationId() {
        return this.activatedRoute.params.subscribe((params: Params) => {
            this.conversationId = params['id'];
        });
    }

    public sendMessage() {
        let data = {
            replied: true,
            text: this.messageText,
            user: this.user.id,
            conversation: this.conversation.id
        };
        this.accountService.sendMessage(data).subscribe(()=> {
            this.userService.updateCurrentUserAccount();
            this.messageText = '';
            this.toastrService.success('Success!', 'Message sent');
            this.getMessages()
        })
    }

}
