import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {Router, ActivatedRoute} from "@angular/router";
import {Http} from '@angular/http';

@Component({
    selector: 'page-item',
    templateUrl: 'activation.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class EmailActivate implements OnInit {
    private baseUrl: string = process.env['API_URL'];
    private key: string;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private http: Http,
                private toastrService: ToastrService,) {}


    ngOnInit() {
        this.route.params.subscribe(params => {
            this.key = params['token'];
        });
        this.http.post(this.baseUrl + 'account/email-activate/',{uid:(this.key).split('$')[0],token:(this.key).split('$')[1]}).subscribe((data) => {
            // console.log(data);
            this.toastrService.success('Success!', 'Email confirmed!');
        }, (error) => {
            this.toastrService.error('Error!', 'Email activation link has expired.');
            this.toastrService.success('Success!', 'New email activation link has been sent!');
        });
        this.router.navigate(['/']);
    }

}
