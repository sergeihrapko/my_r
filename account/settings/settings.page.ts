import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from "../../../service/user.service";
import { User, Region, Country } from "../../../models/index";
import { AuthenticationService } from '../../../service/auth.service';
import { CommonService, CurrencyService, RegionService } from "../../../service/index";
import { Subscription } from "rxjs";
import * as _ from 'underscore'


@Component({
    selector: 'account-page',
    templateUrl: 'settings.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class SettingsPage implements OnInit {

    subscription: Subscription;
    regions: Region[] = [];
    countries: Country[] = [];
    selectedRegion: Region = new Region();
    languages: any[] = [];
    currencies: any[] = [];
    timeZones: any[] = [];

    user: User = new User();
    public accountForm = this.formBuilder.group({
        first_name: ['', [Validators.required]],
        last_name: ['', [Validators.required]],
        region: ['', [Validators.required]],
        country: ['', [Validators.required]],
        phone: ['', [Validators.required]],
        email: ['', [Validators.required]],
        skype: [''],
        company_name: [''],
        company_description: [''],
        vat: [''],
        is_mailing_list: false,
        city: ['', [Validators.required]],
        postcode: ['', [Validators.required]],
        province_state: [''],
        address_line_one: ['', [Validators.required]],
        address_line_two: [''],
    });

    private accountFormError: any = {
        first_name:[],
        last_name:[],
        address_line_one: [],
        city: [],
        postcode: [],
        country: [],
        phone: [],
        region: [],
    };

    requirements_fields : any = {
        first_name: 'First name',
        last_name: 'Last name',
        address_line_one: 'Address line 1',
        city: 'City',
        postcode: 'Postcode',
        country:'Country',
        phone:'Phone',
    };

    private preferredSettingsFormError: any = {
        currency:[],
        timezone: [],
    };

    public securityForm = this.formBuilder.group({
        old_password: ['', [Validators.required]],
        new_password: ['', [Validators.required]],
        repeat_password: ['', [Validators.required]],
    });

    public preferredSettingsForm = this.formBuilder.group({
        // language: ['', [Validators.required]],
        currency: ['', [Validators.required]],
        timezone: ['', [Validators.required]],
    });

    private securityFormError: any = {
        old_password:[],
        new_password: [],
        repeat_password: []
    };

    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private userService: UserService,
                private regionService: RegionService,
                private toastrService: ToastrService,
                private commonService: CommonService,
                private authService: AuthenticationService,
                private currencyService: CurrencyService) {
        this.subscription = this.userService.userChange.subscribe((data) => {
            this.user = data;
        });

    }

    ngOnInit() {
        this.user = this.userService.currentUser;
        this.getPreferredSettingsOptions();
        this.regionService.getRegions().subscribe(
            (data: any) => {
                this.regions = data.json() || [];
            }
        );

        this.regionService.getCountries().subscribe(
            (data: any) => {
                this.countries = data.json() || [];
            }
        )
    }

    private getPreferredSettingsOptions() {
        this.userService.getPreferredSettingsOptions().subscribe(
            (data: any) => {
                data = data.json();
                this.currencies = data['currencies'];
                this.timeZones = data['timezones'];
                this.languages = data['languages'];
            }
        )
    }


    private updateProfile() {
        this.userService.updateProfile(this.accountForm.value, this.user.id).subscribe((data) => {

            let user: any = data.json();

            if (this.userService.currentUser.email !== user.email) {
                console.log('ne raven');
                console.log(this.userService.currentUser.email);
                console.log(user.email);
                this.toastrService.warning('Warning!', 'You need to log in again under the new mail');
                this.toastrService.success('Success!', 'Your profile has been updated');
                this.authService.logout();
            } else {
                this.userService.setCurrentUser(user);
                this.toastrService.success('Success!', 'Your profile has been updated');
                this.accountFormError = {};
            }
        },
            (err) => {
                this.accountFormError = err.json();

                let errors: any = err.json();
                let htmlMessage: string = 'The following fields are missing:';

                for (let field of Object.keys(this.requirements_fields)){
                    if (errors[field]){
                        htmlMessage += '<li>' + this.requirements_fields[field] + '</li>';
                    }
                }

                if ((Object.keys(errors).length === 1) && (errors.email)) {
                    htmlMessage = errors.email + '.';
                }

                if (!this.accountForm.value['country']){
                    this.accountFormError['country'] = ['This field may not be null.'];
                    htmlMessage += '<li>Country</li>';
                }

                this.toastrService.warning(htmlMessage, '', {enableHtml: true});
            });
    }

    dLen(array : any) {
        return (array && array.length > 0) ? true : false;
    }

    private updatePassword() {
        this.userService.updatePassword(this.securityForm.value).subscribe((data) => {
            this.toastrService.success('Success!', 'Security settings was updated');
             this.securityFormError = {}
        },(err) => {
                this.securityFormError = err.json();
                this.toastrService.error('Error!', 'Unable to change security settings');
            })
    }

    showErrorToast() {
        this.toastrService.error('Error!', 'You unauthorized');
    }

    get availableRegions() {
        let country = this.accountForm.value['country'];
        if (country) {
            return this.regions.filter(item => item.countries.indexOf(country) !== -1)
        }

        return this.regions
    }

    get availableCountries() {
        let region = this.accountForm.value['region'];

        if (region) {
             return this.countries.filter(item => item.region == region)
        }
        return this.countries
    }

    updatePreferredSettings() {
        this.preferredSettingsFormError.currency = [];
        if (this.preferredSettingsForm.invalid) {
            this.preferredSettingsFormError.currency = ['This field is required.'];
            return;
        }

        this.userService.updateLocalization(this.preferredSettingsForm.value, this.user.id).subscribe((data) => {
            this.userService.setCurrentUser(data.json());
            this.currencyService.selectCurrency(this.user.currency);
            this.toastrService.success('Success!', 'Your preferred settings have been updated');
        },
            (err) => {
                this.toastrService.error('Error!', 'Please fix error');
            });
    }

    onCountryChange() {
        let selected_country = this.countries.filter(item => item.iso == this.accountForm.value['country'])[0];
        this.user.currency = selected_country.currency;
        this.user.timezone = selected_country.timezone;
    }
}
