import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {AdvertService} from '../../../service/index';
import {CurrencyService} from '../../../service/index';
import {PagerService} from '../../../service/pager.service';
import {DefaultLocale, Currency} from 'angular-l10n';
import {Router, ActivatedRoute, Params, ParamMap} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'listing-page',
    templateUrl: 'listing.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class ListingPage implements OnInit {

    @Currency() currency: string;
    public resultCounter: number = 0;
    private pager: any = {};
    private currentPage: number = 1;
    private preloader: boolean = true;
    private adverts: any[] = [];
    private error: string = null;

    constructor(private advertService: AdvertService,
                private pagerService: PagerService,
                private currencyService: CurrencyService,
                private router: Router,
                private toastrService: ToastrService,) {

    }


    ngOnInit() {
        this.currencyService.onChange.subscribe(() => {

        });
        this.searchAdverts();
        try {
            this.error = this.router.routerState.snapshot.url.split('?')[1].split('=')[1];
            this.router.navigate([this.router.routerState.snapshot.url.split('?')[0]])
        } catch (e) {
        }
    }

    public searchAdverts(paginate: boolean = false) {

        this.preloader = true;

        this.advertService.getUserAdverts(this.currentPage).subscribe(
            (data: any) => {
                this.adverts = data.json().results;
                this.resultCounter = data.json().count;
                this.setPage(this.currentPage, false);
                this.preloader = false;
            }, (err) => {
                this.preloader = false;
            }
        );
    }

    public setPage(page: number, queried: boolean = true) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        this.pager = this.pagerService.getPager(this.resultCounter, page);
        if (queried) {
            this.currentPage = page;
            this.searchAdverts(true);
        }
    }

    public deleteAdvert(advertId: string) {
        this.advertService.deleteAdvert(advertId).subscribe(() => {
            this.adverts = this.adverts.filter((advert) => advert.id !== advertId);
            this.toastrService.success('Success!', 'Advert deleted');
        });
    }

    public openPagePayment(advertId: number) {
        this.router.navigate(['/account/my_listing/' + advertId, {step: 'four'}]);
    }


}
