import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ToastrService} from 'ngx-toastr';
import {AdvertService, UserService, ModalService} from '../../../service/index';
import {CurrencyService} from '../../../service/index';
import {PagerService} from '../../../service/pager.service';
import {DefaultLocale, Currency} from 'angular-l10n';
import {Advert, User} from '../../../models/index'


@Component({
    selector: 'wishList-page',
    templateUrl: 'wishList.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class WishListPage implements OnInit {
    @Currency() currency: string;
    private subscription: Subscription;
    public resultCounter: number = 0;
    private pager: any = {};
    private currentPage: number = 1;
    private preloader: boolean = true;
    private adverts: any[] = [];
    private user:User;

    constructor(private advertService: AdvertService,
                private pagerService: PagerService,
                private currencyService: CurrencyService,
                private toastrService: ToastrService,
                private userService: UserService,
                private modalService: ModalService) {
        this.subscription = this.userService.userChange.subscribe((data) => {
            this.user = data;
        });
    }


    ngOnInit() {
        this.user = this.userService.currentUser;
        this.currencyService.onChange.subscribe(() => {

        });
        this.getWishList()
    }

    public getWishList(paginate: boolean = false) {

        this.preloader = true;

        this.advertService.getUserWishList(this.currentPage).subscribe(
            (data: any) => {
                this.adverts = data.json().results;
                this.resultCounter = data.json().count;
                this.preloader = false;
                this.setPage(this.currentPage, false)
            }
        );
    }

    public setPage(page: number, queried: boolean = true) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        this.pager = this.pagerService.getPager(this.resultCounter, page);
        if (queried) {
            this.currentPage = page;
            this.getWishList(true);
        }


    }

    private deleteFromWishList(userId: number, advert: Advert) {
        // console.log(1)
        let index = advert['users_wishes'].indexOf(userId);
        advert['users_wishes'].splice(index, 1);


        this.advertService.addToWishList(advert['id'], {"users_wishes": advert['users_wishes']}).subscribe(() => {
            this.toastrService.success('Success!', advert.machine_name + ' was removed from wish list');
            this.currentPage = 1;
            this.getWishList();
        })
    }


}
