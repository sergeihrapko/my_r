import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AdvertService, UserService } from '../../service/index';
import { CurrencyService, ModalService } from '../../service/index';
import { PagerService } from '../../service/pager.service';
import { IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { ActiveCategory, SearchParams, FilterParams } from '../../models/index';
import { LightCategory, User } from '../../models/index';
import { DefaultLocale, Currency } from 'angular-l10n';
import { AddTotalPipe } from '../../pipes/index';
import { Subscription } from 'rxjs';
import { Advert } from '../../models/advert';
import * as moment from 'moment';

@Component({
    selector: 'adverts',
    templateUrl: './adverts.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class AdvertsComponent implements OnInit {

    userSubscription: Subscription;
    querySubscription: Subscription;
    activeCategorySubscription: Subscription;
    ownerSubscription: Subscription;
    @DefaultLocale() defaultLocale: string;
    @Currency() currency: string;

    public searchParams: SearchParams = new SearchParams();
    public selectedCategories: Array<any> = [];
    public filterParams = new FilterParams();
    public adverts: any = [];
    public isShowNoAdvertsMessage: boolean = false;

    public resultCounter: number = 0;

    public selectSettings: IMultiSelectSettings = {
        enableSearch: true,
        checkedStyle: 'fontawesome',
        buttonClasses: 'btn btn-default btn-block',
        displayAllSelectedText: false,
        closeOnSelect: true,
        dynamicTitleMaxItems: 1,
    };
    public isPriceFormCollapsed = true;
    private pager: any = {};
    private preloader: boolean = true;
    public pageSizes: number[] = [9, 18, 36, 72];
    public view: string = 'grid';
    public resultOrdering: any[] = [
        {
            key: '-created',
            value: 'Created: newest first'
        },
        {
            key: 'created',
            value: 'Created: oldest first'
        },
        {
            key: 'price',
            value: 'Price: low to high'
        },
        {
            key: '-price',
            value: 'Price: high to low'
        },
        {
            key: 'machine__model__manufacturer__name',
            value: 'Manufacturer: A-Z'
        },
        {
            key: '-machine__model__manufacturer__name',
            value: 'Manufacturer: Z-A'
        },
        {
            key: 'machine__model__name',
            value: 'Model: A-Z'
        },
        {
            key: '-machine__model__name',
            value: 'Model: Z-A'
        },
        {
            key: 'country',
            value: 'Country: A-Z'
        },
        {
            key: '-country',
            value: 'Country: Z-A'
        },
        {
            key: '-year',
            value: 'Year: newest first'
        },
        {
            key: 'year',
            value: 'Year: oldest first'
        }
    ];

    constructor(private router: Router,
                private advertService: AdvertService,
                private pagerService: PagerService,
                private currencyService: CurrencyService,
                private addTotalpipe: AddTotalPipe,
                private userService: UserService,
                private toastrService: ToastrService,
                private modalService: ModalService) {
        this.selectSettings['searchEmptyResult'] =  'Nothing available...';

        this.activeCategorySubscription = this.advertService.activeCategoryChange.subscribe((data) => {
            if (data) {
                this.setActiveCategory(true);
                this.setOwner(true);
            }
        });

        this.ownerSubscription = this.advertService.searchOwnerChange.subscribe((data) => {
            this.searchParams.owner = data;
            this.searchAdverts();
        });

        this.querySubscription = this.advertService.searchQueryChange.subscribe((data) => {
            this.searchParams.query = data;
            this.searchAdverts();
        });
    }

    ngOnInit() {
        this.searchParams.ordering = ['-urgent', '-created'];
        this.searchParams.query = this.advertService.query;
        if (!!this.advertService.owner) {
            this.searchParams.owner = this.advertService.owner.id;
        }
        this.currencyService.onChange.subscribe(() => {
            this.searchAdverts();
        });
        this.setActiveCategory();
        this.getFilterParams();
        this.searchAdverts();
    }

    public searchAdverts(paginate: boolean = false) {
        //  reset page number on new search params
        console.log(this.searchParams)
        this.searchParams.page = 1;
        this.saveSearchParams();
        this.preloader = true;
        this.advertService.searchAdvert(this.searchParams).subscribe(
            (data: any) => {
                this.adverts = data.json().results;
                this.resultCounter = data.json().count;
                let filteredIds: string[] = [];
                data.json().results.map((val: any, key: number) => {
                    filteredIds.push(val.id);
                });
                console.log('searchAdverts');
                localStorage.setItem('filteredIds', JSON.stringify({[this.searchParams.page] : filteredIds}));
                localStorage.setItem('filterPages', JSON.stringify({
                    next: data.json().next,
                    previous: data.json().previous
                }));
                this.isShowNoAdvertsMessage = this.resultCounter < 1;
                if (!paginate) {
                    this.searchParams.page = 1;
                    this.setPage(this.searchParams.page, false);
                }
                this.preloader = false;
                this.advertService.activeCategory = null;
                this.getFilterParams();
            }, (err) => {
                this.preloader = false;
            }
        );

    }

    public getFilterParams() {
        this.advertService.getAdvertFilter(this.searchParams).subscribe(
            (data: any) => {
                this.filterParams = data.json();
                for (let key in this.filterParams) {
                    this.addTotalpipe.transform(this.filterParams[key]);
                }
                console.log('this.filterParams', this.filterParams)
            }
        );
    }

    public deleteSearchParam(name: any, param: string) {

        this.searchParams[param] = this.searchParams[param].filter((item: any) => {
            return item !== name;
        });
        this.searchAdverts();
    }

    private setActiveCategory(search: boolean = false) {

        let activeCategory = this.advertService.activeCategory;

        if (activeCategory) {

            this.searchParams.category = activeCategory.id;
            if (search) {
                this.selectedCategories = [];
            }
            this.selectedCategories.push(activeCategory);

        }
        if (search) {
            this.searchAdverts();
        }
    }

    private setOwner(search: boolean = false) {
        let user = this.advertService.owner;
        if (user) {
            this.searchParams.owner = user.id;
        }
        if (search) {
            this.searchAdverts();
        }
    }

    public selectCategory(category: any) {
        this.searchParams.category = category.id;
        this.selectedCategories.push(category);
        this.searchAdverts();
    }

    public removeCategory(index: number) {
        this.selectedCategories = this.selectedCategories.splice(0, index);
        if (index > 0) {
            this.searchParams.category = this.selectedCategories[this.selectedCategories.length - 1]['id'];
        }
        else {
            this.searchParams.category = null;
        }
        this.searchAdverts();
    }


    public setPage(page: number, queried: boolean = true) {
        this.pager = this.pagerService.getPager(this.resultCounter, page, this.searchParams.page_size);
        this.searchParams.page = page;
        if (queried) {
            this.searchAdverts(true);
        }
    }

    private saveSearchParams() {
        localStorage.setItem('searchParams', JSON.stringify(this.searchParams));
    }

    private addToWishList(userId: number, advert: Advert, type: string = 'add') {
        let successMessage: string = '';
        if (type == 'remove') {
            let index = advert['users_wishes'].indexOf(userId);
            advert['users_wishes'].splice(index, 1);
            successMessage = advert.machine_name + ' was removed from wish list';

        }
        else {
            advert['users_wishes'].push(userId);
            successMessage = advert.machine_name + ' was added to wish list';
        }

        this.advertService.addToWishList(advert['id'], {'users_wishes': advert['users_wishes']}).subscribe(() => {
            this.toastrService.success('Success!', successMessage);
        });
    }


    get availableManufacturers() {
        if (this.searchParams.model.length === 0) {
            return this.filterParams.manufacturer;
        }
        const manufacturersIds = this.filterParams.model.filter(item => this.searchParams.model.indexOf(item.id) !== -1).map(item => item.manufacturer);
        return this.filterParams.manufacturer.filter(item => manufacturersIds.indexOf(item.id) !== -1);
    }

    get availableRegions() {
        if (this.searchParams.country.length === 0) {
            return this.filterParams.region;
        }
        const regionsIds = this.filterParams.country.filter(item => this.searchParams.country.indexOf(item.id) !== -1).map(item => item.region);
        return this.filterParams.region.filter(item => regionsIds.indexOf(item.id) !== -1);
    }

    public clearQuery() {
        this.searchParams.query = '';
        this.advertService.setSearchQuery(this.searchParams.query);
    }

    public clearOwner() {
        this.searchParams.owner = undefined;
        this.advertService.setSearchOwner(this.searchParams.owner);
    }

    ngOnDestroy() {
        localStorage.removeItem('searchParams');
        this.clearQuery();
    }

    public openPagePayment(advertId: number) {
        this.router.navigate(['/account/my_listing/' + advertId, { step: 'four' }]);
    }

    onScroll () {
        this.searchParams.page += 1;
        this.loadAdverts();
    }

    loadAdverts(paginate: boolean = false) {
        this.saveSearchParams();
        this.preloader = true;
        this.advertService.searchAdvert(this.searchParams).subscribe(
            (data: any) => {
                this.adverts = this.adverts.concat(data.json().results);
                this.resultCounter = data.json().count;
                this.isShowNoAdvertsMessage = this.resultCounter < 1;
                this.addFilteredIds(data);
                this.setPage(this.searchParams.page, false);
                this.preloader = false;
                this.advertService.activeCategory = null;
                this.getFilterParams();
            }, (err) => {
                this.preloader = false;
            }
        );
    }

    addFilteredIds(data: any) {
        let pageNumberRaw = /page=([\d]+)/.exec(data.url)[0];
        let pageNumber = /([\d]+)/.exec(pageNumberRaw)[0];
        let filteredIds: string[] = [];
        let ids: any = {};
        ids = JSON.parse(localStorage.getItem('filteredIds'));
        data.json().results.map((val: any, key: number) => {
            filteredIds.push(val.id);
        });
        ids[pageNumber] = filteredIds;
        localStorage.setItem('filteredIds', JSON.stringify(ids));
        localStorage.setItem('filterPages', JSON.stringify({
            next: data.json().next,
            previous: data.json().previous
        }));
    }

    formatDate(date: string){
        return moment(date, 'DD-MM-YYYY').format('MMM DD, YYYY');
    }

    passActiveCategory(category: any) {
        this.advertService.setActiveCategory(category)
    }
}

