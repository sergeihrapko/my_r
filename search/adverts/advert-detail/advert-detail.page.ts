import { Component, OnChanges, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

import * as moment from 'moment';

import * as _ from 'underscore';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ShareButton, ShareProvider } from 'ngx-sharebuttons';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from 'ngx-gallery';

import { Advert, User } from '../../../models/index';
import { CommonService } from '../../../service/common.service';
import { CurrencyService } from '../../../service/currency.service';
import { AccountService, AdvertService, ModalService, ReviewsService, UserService } from '../../../service/index';

import { Choices } from '../../../models/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { MachineReviewModalComponent } from '../../../components/modals/index';
import {query} from "@angular/animations";


@Component({
    selector: 'advert-detail',
    templateUrl: 'advert-detail.tpl.pug',
    encapsulation: ViewEncapsulation.None,
})
export class AdvertDetailPage implements OnInit, OnDestroy {

    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
    private subscription: Subscription;
    public advert: Advert = new Advert();
    public advertId: number;
    public minCharsToTrunc: number = 200;
    public minCharsToTruncCompanyDesc: string = "350";
    public reviews: Array<any> = [];
    public terms: string;
    private subscribe_route: any;
    private relatedAdverts: Advert[] = [];
    private recentlyViewedAdverts: Advert[] = [];
    private user: User;
    public currencyCode: string = '';
    public mainImageChangeSubscription: Subscription;
    public mainImageUrl: string = '';
    public advertTypeChoices: Choices[] = [];
    public userCanLeaveReview: boolean = true;
    public canNext: boolean = true;
    public canPrevious: boolean = true;
    public currentPage: number = -1;
    public index: number = -1;
    private fbButton: any;
    private twButton: any;
    private ldnButton: any;
    public filteredIds: any;
    public pageChecked: boolean = false;

    public advertCategories: any[] = [];

    public termsList = [
        {'value': 'EXW', 'title': 'EXW (Ex-Works)'},
        {'value': 'LOT / LOC', 'title': 'LOT / LOC (Loaded on Transport / Container)'},
        {'value': 'CFR', 'title': 'CFR (Cost and Freight)'},
        {'value': 'CIF', 'title': 'CIF (Cost, Freight and Insurance)'},
        {'value': 'FOB', 'title': 'FOB (Free on Board)'},
        {'value': 'W', 'title': 'W (Warranty)'},
        {'value': 'D&I', 'title': 'D&I (Delivered & Installed)'}
    ];

    constructor(private advertService: AdvertService,
                private reviewsService: ReviewsService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private toastrService: ToastrService,
                private userService: UserService,
                private accountService: AccountService,
                private commonService: CommonService,
                private currencyService: CurrencyService,
                private modalService: ModalService,
                private ngbModalService: NgbModal) {
        this.subscription = this.userService.userChange.subscribe((data) => {
            this.user = data;
        });
    }

    ngOnDestroy() {
        this.subscribe_route.unsubscribe();
    }

    public setSearchCategory(category: any) {
        return this.advertService.setActiveCategory(category);
    }

    ngOnInit() {
        this.user = this.userService.currentUser;

        this.subscribe_route = this.activatedRoute.params.subscribe((params: Params) => {
            this.advertId = parseInt(params['id']);
            this.advertService.pushRecentlyViewed(this.advertId);
            this.getAdvert(this.advertId);
        });
        let filteredIds = JSON.parse(localStorage.getItem('filteredIds'));
        if (!filteredIds) {
            this.filteredIds = {
                '1': [this.advertId]
            }
        } else {
            this.filteredIds = filteredIds
        }
        console.log(this.filteredIds);
        this.getSwitcherData();

        this.currencyService.onChange.subscribe(() => {
            this.getAdvert(this.advertId);
            this.currencyCode = this.currencyService.getCurrency();
        });


        this.mainImageChangeSubscription = this.commonService.currentImageSrcChange.subscribe(
            (data) => {
                this.mainImageUrl = data;
            }
        );

        this.advertService.getAdvertChoices().subscribe(
            success => {
                let choices = success.json();
                this.advertTypeChoices = choices.advert_type;
            },
            errors => {
                this.advertTypeChoices = [
                    {value: '0', title: 'New'},
                    {value: '1', title: 'Used'},
                ];
            }
        );

        this.getRecentlyViewedAdvert();

        this.fbButton = new ShareButton(
            ShareProvider.FACEBOOK,
            '<i class="fa fa-facebook"></i>',
            'facebook'
        );

        this.twButton = new ShareButton(
            ShareProvider.TWITTER,
            '<i class="fa fa-twitter"></i>',
            'twitter'
        );

        this.ldnButton = new ShareButton(
            ShareProvider.LINKEDIN,
            '<i class="fa fa-linkedin"></i>',
            'linkedin'
        );

        this.galleryOptions = [
            {
                width: '100%',
                height: '600px',
                thumbnails: false,
                thumbnailsColumns: 4,
                thumbnailsMargin: 50,
                thumbnailMargin: 20,
                thumbnailsPercent: 25,
                imageArrowsAutoHide: true,
                previewKeyboardNavigation: true,
                imageAnimation: NgxGalleryAnimation.Slide
            },
            {
                breakpoint: 800,
                width: '100%',
                height: '600px',
                imagePercent: 80,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20
            },
            {
                breakpoint: 400,
                preview: false
            }
        ];
    }

    checkReviewLeft(ownerId: any, reviewerId: any) {
        this.reviewsService.getReviewLeft(ownerId, reviewerId).subscribe((data) => {
            let result: any = data.json();
            this.userCanLeaveReview = result['can-leave-review'];
        });
    }


    public getAdvert(id: number) {
        this.advertService.getAdvertDetail(id).subscribe(
            (data) => {
                this.advert = data.json();
                this.galleryImages = [];

                for (let image of this.advert.images) {
                    if (image.url) {
                        this.galleryImages.push({
                            small: image.url,
                            medium: image.url,
                            big: image.url
                        });
                    }
                }

                let nextCategory =  this.advert.category;
                this.advertCategories = [];
                while (nextCategory !== null) {
                    this.advertCategories.push({
                        'id': nextCategory.id,
                        'name': nextCategory.name
                    });
                    nextCategory = nextCategory.parent
                }
                this.advertCategories.reverse();
                console.log(this.advertCategories)
                if (this.advert.images.length > 1) {
                    this.galleryOptions[0].thumbnails = true;
                }

                if (this.advert.main_image) {
                    this.mainImageUrl = this.advert.main_image['url'];
                }
                this.getRelatedAdvert(data.json().model_id, data.json().manufacturer_id);
                this.getMachineReviews(data.json()['machine']['id']);

                for (let term of this.termsList){
                    if (term.value === this.advert.terms) {
                        this.terms = term.title;
                    }
                }

                if (this.user) {
                    this.checkReviewLeft(this.advert.owner.id, this.user.id);
                }
            },
            (err) => {
                this.router.navigate(['/not_found']);
            }
        );
    }


    private getRelatedAdvert(model: number, manufacturer: number) {
        let query = {
            manufacturer: manufacturer,
            category: '',
            model: model,
        };
        this.advertService.getRelatedAdvert(query).subscribe((data) => {
            let results = data.json().results;
            let selfNumb = -1;
            for (let i in results){
                if (results[i].id === this.advertId){
                    selfNumb = parseInt(i);
                }
            }
            if (selfNumb !== -1) {
                results.splice(selfNumb, 1);
            }
            this.relatedAdverts = results;
        });
    }

    private getRecentlyViewedAdvert() {
        let query = {
            id: JSON.parse(localStorage.getItem('recentlyViewed'))
        };
        this.advertService.searchAdvert(query).subscribe((data) => {
            this.recentlyViewedAdverts = data.json().results;
        });
    }

    private getMachineReviews(id: any) {
        this.reviewsService.getMachineReviews('machine', id).subscribe((data) => {
            this.reviews = data.json();
        });
    }

    private addToWishList(userId: number, advert: Advert, type: string = 'add') {
        let successMessage: string = '';
        if (type === 'remove') {
            let index = advert['users_wishes'].indexOf(userId);
            advert['users_wishes'].splice(index, 1);
            successMessage = advert.machine_name + ' was removed from wish list';

        }
        else {
            advert['users_wishes'].push(userId);
            successMessage = advert.machine_name + ' was added to wish list';
        }

        this.advertService.addToWishList(advert['id'], {'users_wishes': advert['users_wishes']}).subscribe(() => {
            this.toastrService.success('Success!', successMessage);
        });
    }

    private createAlert() {
        this.accountService.createAlert(this.advert.manufacturer_id, this.advert.model_id).subscribe(() => {
            this.toastrService.success('success!', 'Alert created');
        }, (error: any) => {
            for (let item of error.json()['non_field_errors']) {
                this.toastrService.error('Error!', item);
            }
        });
    }

    private makeUserSearch() {
        this.advertService.setSearchOwner(this.advert.owner);
        this.router.navigate(['adverts']);
    }

    private openMap(content: any) {
        this.ngbModalService.open(content, {size: 'lg', windowClass: 'mapModal'}).result.then();
    }

    private getAdvertType() {
        if (!!this.advert && this.advertTypeChoices.length > 0) {
            let result = _.find(this.advertTypeChoices, iterItem => {
                return iterItem.value == this.advert.advert_type;
            });
            return result.title;
        }
        else {
            return '';
        }
    }

    private checkAvailability() {
        let result = false;
        if (!!this.advert.availability) {
            let today = new Date();
            let availability_date = new Date(this.advert['availability_date']);
            result = availability_date > today;
        }
        return result;
    }

    // public contactSeller() {
    //     const modalRef = this.modalService.open(MessageModalComponent, {windowClass: 'auth-modal'});
    //     modalRef.componentInstance.userId = this.user['id'];
    //     modalRef.componentInstance.recipient = this.advert['owner']['id'];
    //     modalRef.componentInstance.advertId = this.advertId;
    //
    // }
    // public openSignInModal() {
    //     const modalRef = this.modalService.open(SigninModalComponent, { windowClass: 'auth-modal' });
    // }

    public openReviewModal(userId: number) {
        const modalRef = this.ngbModalService.open(MachineReviewModalComponent, { windowClass: 'auth-modal' });
        modalRef.componentInstance.userId = userId;
        modalRef.componentInstance.machineId = this.advert['machine']['id'];
        modalRef.componentInstance.byOwner = false;

        modalRef.result.then((result) => {
            this.getMachineReviews(this.advert['machine']['id']);
        });
    }

    public formatDate(date: string) {
        return moment(date, 'DD-MM-YYYY').format('MMMM DD, YYYY');
    }

    public printAdvert() {
        window.print();
    }

    /////////

    goNext() {
        let array: number[] = this.filteredIds[this.currentPage];
        if (!array) {
            this.checkButtons();
            return;
        }
        if (this.index !== array.length - 1) {
            this.index++;
            this.router.navigate([`/adverts/${array[this.index]}`]);
            this.checkButtons();
        } else {
            let nextPage = this.currentPage + 1;
            array = this.filteredIds[nextPage];
            this.pageChecked = false;
            if (array) {
                let newIndex = 0;
                this.router.navigate([`/adverts/${array[newIndex]}`]);
                this.currentPage = nextPage;
                this.index = newIndex;
                this.checkButtons();
            } else {
            }
        }
    }

    goBack() {
        let array: number[] = this.filteredIds[this.currentPage];
        if (!array) {
            this.checkButtons();
            return;
        }
        if (this.index !== 0) {
            this.index--;
            console.log('going to prev elem, params: index', this.index);
            this.router.navigate([`/adverts/${array[this.index]}`]);
        } else {
            this.currentPage--;
            array = this.filteredIds[this.currentPage];
            console.log('first, going to prev array, params: array, index', array)
            if (array) {
                this.index = array.length - 1;
                this.router.navigate([`/adverts/${array[this.index]}`]);
            } else {
                this.currentPage++;
            }
        }
        this.checkButtons();
    }

    getSwitcherData() {
        Object.keys(this.filteredIds).map((val: string, key: number) => {
            const index = this.filteredIds[val].indexOf(this.advertId);
            if (index !== -1) {
                this.currentPage = parseInt(val);
                this.index = index;
            }
        });
        if (this.currentPage === -1) {
            this.currentPage = 1;
            this.index = 0;
            this.filteredIds['1'] = [this.advertId].concat(this.filteredIds['1']);
        }
        this.checkButtons();
    }

    checkButtons() {
        let array: number[] = this.filteredIds[this.currentPage];
        if (array) {
            if (array[this.index + 1]) {
                this.canNext = true;
                if (this.index + 1 === array.length - 1) {
                    this.checkPageAndAdd(this.currentPage, this.currentPage + 1, 'next');
                }
            } else {
                let nextPage = this.currentPage + 1;
                let nextArray = this.filteredIds[nextPage];
                console.log('nextarray', nextArray)
                if (nextArray) {
                    this.canNext = true;
                } else {
                    if (this.pageChecked) {
                        this.canNext = false;
                    } else {
                        this.checkPageAndAdd(this.currentPage, nextPage, 'next');
                    }
                }
            }

            if (array[this.index - 1]) {
                this.canPrevious = true;
            } else {
                let prevPage = this.currentPage - 1;
                let prevArray = this.filteredIds[prevPage];
                if (prevArray) {
                    this.canPrevious = true;
                } else {
                    if (prevPage <= 1) {
                        this.canPrevious = false;
                    }
                }
            }
        }
        // this.getPage();
    }

    checkPageAndAdd(page: any, nextPage: any, type: any) {
        let searchQuery = JSON.parse(localStorage.getItem('searchParams'))
        if (!searchQuery) {
            console.log('Get data for new array, if no arrays here')
            this.advertService.searchAdvert({page: this.currentPage}).subscribe(response => {
                let newIds: number[] = [];
                response.json().results.map((val: any, key: number) => {
                    newIds.push(val.id);
                });
                this.filteredIds[page] = this.filteredIds[page].concat(newIds);
                this.pageChecked = true;
                if (type === 'next') {
                    this.canNext = true;
                } else {
                    this.canPrevious = false;
                }
                localStorage.setItem('searchQuery', JSON.stringify({page: this.currentPage}));
                console.log('Got data for new array, if no arrays here', this.filteredIds)
            });
            return
        }
        searchQuery.page = nextPage;
        this.advertService.searchAdvert(searchQuery).subscribe(response => {
                let newIds: number[] = [];
                response.json().results.map((val: any, key: number) => {
                    newIds.push(val.id);
                });
                this.filteredIds[nextPage] = newIds;
                this.pageChecked = true;
                if (type === 'next') {
                    this.canNext = true;
                } else {
                    this.canPrevious = false;
                }
            },
            error => {
                this.pageChecked = true;
                console.log(error);
            }).unsubscribe();
    }
}
