from django.urls import path
from .views import CreateUserApiView,DetailUserAPIView
from rest_framework_jwt.views import obtain_jwt_token


urlpatterns = [
    path('users/',CreateUserApiView.as_view(),name='create_user_api_view'),
    path('users/<int:user_id>/',DetailUserAPIView.as_view(),name='detail_user'),
    path('login/', obtain_jwt_token),

]