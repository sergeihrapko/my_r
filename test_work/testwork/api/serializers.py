from rest_framework import serializers
from users.models import User


class UserSerializers(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'email',
            'last_name',
            'first_name',
            'password'
        ]
        read_only_fields = ['is_staff', 'is_superuser']
        extra_kwargs = {"password": {"write_only": True}}

    def save(self):
        user = super().save(**self.validated_data)
        user.set_password(self.validated_data['password'])
        user.save()
        return user


class UpdateUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'email',
            'last_name',
            'first_name',
            'phone',
        ]
        read_only_fields = ['is_staff', 'is_superuser']
