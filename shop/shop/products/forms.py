from django import forms
from django.core.exceptions import ValidationError
from .models import Book


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            'name',
            'cover_image',
            'price',
            'authors',
            'serie',
            'genres',
            'published_year',
            'pages',
            'binding',
            'book_format',
            'isbn',
            'weight',
            'age_restriction',
            'publisher',
            'printer',
            'available_books',
            'rate',
            'e_mail',

        ]


