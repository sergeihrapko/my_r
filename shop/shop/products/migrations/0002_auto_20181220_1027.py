# Generated by Django 2.1.4 on 2018-12-20 07:27

from django.db import migrations, models
import products.models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='e_mail',
            field=models.EmailField(blank=True, max_length=30, verbose_name='EMAIL'),
        ),
        migrations.AlterField(
            model_name='book',
            name='isbn',
            field=models.CharField(max_length=50, validators=[products.models.valid_isbn], verbose_name='Формат'),
        ),
    ]
