from django.db import models
from django.core.exceptions import ValidationError
from reference.models import(
    Author,
    Serie,
    Genre,
    PublishingHouse,
    PrintingHouse)

def valid_isbn(isbn):
    if len(isbn) < 13:
        raise ValidationError("Число символов должно быть больше 13")



class Book(models.Model):
    name = models.CharField(
        "Название книги",
        max_length=200,
        )
    cover_image = models.ImageField(
        "Фото обложки")
    price = models.DecimalField(
        "Стоимость",
        max_digits=5,
        decimal_places=2)
    authors = models.ManyToManyField(Author, verbose_name="Авторы")
    serie = models.ForeignKey(
        Serie,
        verbose_name="Серия",
        on_delete=models.CASCADE)
    genres = models.ManyToManyField(Genre, verbose_name="Жанры"
                                    ,related_name='count_genre')
    published_year = models.DateField("Год издания")
    pages = models.IntegerField("Страниц")
    binding = models.CharField("Переплет", max_length=50)
    book_format = models.CharField("Формат", max_length=50)
    isbn = models.CharField("Формат", max_length=50,
                            validators=[valid_isbn])
    weight = models.IntegerField("Вес гр.")
    age_restriction = models.CharField("Возрастные ограничения", max_length=4)
    publisher = models.ForeignKey(
        PublishingHouse,
        verbose_name="Издательство",
        on_delete=models.CASCADE)
    printer = models.ForeignKey(
        PrintingHouse,
        verbose_name="Производитель",
        on_delete=models.CASCADE)
    available_books = models.IntegerField(
        "Книг в наличии",
        default=0
    )
    active = models.BooleanField(
        "Товар активный",
        default=True
    )
    rate = models.FloatField(
        "Рейтинг",
        default=0
    )
    created_date = models.DateTimeField(
        "Дата создания",
        auto_now=False,
        auto_now_add=True
    )
    updated_date = models.DateTimeField(
        "Дата изменения",
        auto_now=True,
        auto_now_add=False
    )

    e_mail = models.EmailField(
        "Email",
        max_length=30,
        blank=False,
    )

    def __str__(self):
        return self.name