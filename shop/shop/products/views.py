from django.shortcuts import render
from django.views.generic import CreateView,UpdateView,TemplateView,DeleteView,ListView,DetailView
from .forms import BookForm
from .models import Book

# Create your views here.


class ListBookView(ListView):
    template_name = 'book/book_page.html'
    model = Book


class CreateBookView(CreateView):
    template_name = 'book/book_add.html'
    form_class = BookForm
    success_url = '/admin_panel/products/'


class UpdateBookView(UpdateView):
    template_name = 'book/book_update.html'
    form_class = BookForm
    model = Book
    success_url = '/admin_panel/products/'


class DeleteBookView(DeleteView):
    template_name = 'book/book_delete.html'
    form_class = BookForm
    model = Book
    success_url ='/admin_panel/products/'


class DetailBookView(DetailView):
    template_name = 'book/book_detail_page.html'
    model=Book