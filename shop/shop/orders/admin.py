from django.contrib import admin
from orders import models


class OrderAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "cart",
        "status",
        "created_date",
        "updated_date"
    )

    class Meta:
        model = models.Order

admin.site.register(models.Order, OrderAdmin)
