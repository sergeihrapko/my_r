from django.db import models
from django.contrib.auth import get_user_model
from cart.models import Cart
from reference.models import OrderStatus


class Order(models.Model):
    cart = models.ForeignKey(
        Cart,
        verbose_name="Корзина",
        on_delete=models.PROTECT)
    status = models.ForeignKey(
        "reference.OrderStatus",
        verbose_name="Статус заказа",
        on_delete=models.PROTECT)
    phone = models.CharField(
        verbose_name="Контактный телефон",
        help_text="+37529-123-45-67",
        max_length=16)
    email = models.EmailField(
        verbose_name="Электронная почта",
        null=True,
        blank=True,
        help_text="user@mail.com")
    delivery_address = models.TextField(
        "Адрес доставки",
        null=True,
        blank=True,
        help_text="Минск, ул. Ленина, 3-12"
        )
    comments = models.TextField(
        "Дополнительная информация",
        null=True,
        blank=True
        )
    created_date = models.DateTimeField(
        "Дата создания",
        auto_now=False,
        auto_now_add=True)
    updated_date = models.DateTimeField(
        "Дата изменения",
        auto_now=True,
        auto_now_add=False)

    def __str__(self):
        return "Заказ № {}, от {}".format(
            self.pk,
            self.created_date.strftime('%d.%m.%Y'))
