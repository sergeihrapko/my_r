# Generated by Django 2.1.4 on 2019-01-09 08:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='delivery_address',
            field=models.TextField(blank=True, help_text='Минск, ул. Ленина, 3-12', null=True, verbose_name='Адрес доставки'),
        ),
        migrations.AddField(
            model_name='order',
            name='phone',
            field=models.CharField(default='+375(29)123-45-67', help_text='+37529-123-45-67', max_length=16, verbose_name='Контактный телефон'),
            preserve_default=False,
        ),
    ]
