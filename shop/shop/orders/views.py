from django.views.generic import (
    UpdateView, DetailView, ListView,
    DeleteView, CreateView)
from django.urls import reverse_lazy

from .forms import CheckoutOrderForm
from .models import Order


class OrderCheckoutView(CreateView):
    model = Order
    template_name = 'cart/view-cart.html'
    form_class = CheckoutOrderForm

    def get_success_url(self):
        del self.request.session['cart_id']
        return reverse_lazy('orders:success', kwargs={'pk': self.object.pk})


class OrderCheckoutSuccess(DetailView):
    model = Order
    template_name = 'orders/success.html'


class OrdersListAdmin(ListView):
    model = Order
    template_name = 'orders/list-admin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["active"] = "orders"
        context["total"] = self.object_list.count()
        return context
    
