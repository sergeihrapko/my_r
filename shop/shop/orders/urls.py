from django.urls import path

from . import views

app_name = "orders"

urlpatterns = [
    path(
        'check-out',
        views.OrderCheckoutView.as_view(),
        name="check-out"),
    path(
        'success/<int:pk>/',
        views.OrderCheckoutSuccess.as_view(),
        name="success"),
    # path(
    #     'list/',
    #     views.OrdersList.as_view(),
    #     name="list")
    path(
        'list-admin/',
        views.OrdersListAdmin.as_view(),
        name="list-admin")
]
