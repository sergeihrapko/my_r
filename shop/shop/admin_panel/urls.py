from django.urls import path
from . import views


urlpatterns = [
    path('',views.index,name='admin_panel'),
    path('contact/',views.about,name="about"),
    path('statistics/', views.statistics, name="statistika")

]