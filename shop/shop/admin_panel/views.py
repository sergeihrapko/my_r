from django.shortcuts import render
from django.http import HttpResponse
from products.models import Book
from datetime import datetime,timedelta
from django.db.models import Count
from reference.models import Genre
from django.db.models.functions import TruncMonth
# Create your views here.

def index(request):
  return render(request,'admin_panel.html')

def about (request):

  return render(request,"about.html",{'contact':['Если у вас есть вопросы звоните по телефону','+375333627282','Или пишите на мыло:','ykaz6@yandexru'],})

def statistics(request):
  count_all=Book.objects.all().count()
  count_active=Book.objects.filter(active="True").count()
  end_date = datetime.now().replace(hour=0,minute=0,second=0,microsecond=0)
  start_date=end_date - timedelta(days=1)
  tommorow_count= Book.objects.filter(created_date__gte=start_date,created_date__lt=end_date).count()
  count_all_genres = Genre.objects.all().count()
  count_books_in_ganre=Genre.objects.annotate(number_of_entries=Count('count_genre'))
  c_b_i_g1= count_books_in_ganre[0].number_of_entries
  c_b_i_g2 = count_books_in_ganre[1].number_of_entries
  month_count = Book.objects.annotate(month=TruncMonth('created_date')).count()


  return render(request,"statistika.html",{'count_all':count_all,'count_active':count_active,'tommorow_count':tommorow_count,'count_all_genres':count_all_genres,
                                           'c_b_i_g1':c_b_i_g1,'c_b_i_g2':c_b_i_g2,'month_count':month_count})
