from django.urls import path
from . import views
from .views import RegisterFormView,LoginFormView,LogoutView


urlpatterns =  [
    path('',views.my_index, name = 'my_index'),
    path('proba/',views.my_index1, name = 'proba'),
    path('/about',views.about_site,name='about_site'),
    path('delivery',views.delivery,name='delivery'),
    path('reg/', RegisterFormView.as_view(), name='reg'),
    path('auth/', LoginFormView.as_view(), name='auth'),
    path('logout/', LogoutView.as_view(), name='out'),
    path('success_page/', views.success, name='success_page'),

]

