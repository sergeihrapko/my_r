from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.contrib.auth import logout

# Create your views here.

def my_index (request):
    return render(request,'index.html')

def my_index1 (request):
    return render(request,'index.html')

def about_site (request):
    return render(request,'about.html')

def delivery(request):
    return render(request,'delivery.html')


#регистрация
class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/success_page/"
    template_name = "register.html"

    def form_valid(self, form):
        form.save()

        # Вызываем метод базового класса
        return super(RegisterFormView, self).form_valid(form)


#аутентификация
class LoginFormView(FormView):
    form_class = AuthenticationForm

    # Аналогично регистрации, только используем шаблон аутентификации.
    template_name = "auth.html"

    # В случае успеха перенаправим на главную.
    success_url = "/"

    def form_valid(self, form):
        # Получаем объект пользователя на основе введённых в форму данных.
        self.user = form.get_user()

        # Выполняем аутентификацию пользователя.
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)
#выход

class LogoutView(View):
    def get(self, request):
        # Выполняем выход для пользователя, запросившего данное представление.
        logout(request)

        # После чего, перенаправляем пользователя на главную страницу.
        return HttpResponseRedirect("/")

def success (request):
    return render(request,'success_page.html')
