from django.views.generic import CreateView, UpdateView ,TemplateView, DeleteView,ListView,DetailView
from .forms import AuthorForm,GenreForm,SerieForm,PublishingHouseForm,PrintingHouseForm
from .models import Author,Genre,Serie,PublishingHouse,PrintingHouse
from django.views import View
from django.shortcuts import render


# Авторы


class SpravkaView(TemplateView):
    template_name = 'home_page.html'

class ListAuthorView(ListView):
    template_name = 'author/author_page.html'
    model = Author

class DetailAuthorView(DetailView):
    template_name = 'author/author_detail_page.html'
    model = Author

class CreateAuthorView(CreateView):
    template_name = 'author/author_add.html'
    form_class = AuthorForm
    success_url = '/admin_panel/reference/author/'


class UpdateAuthorView(UpdateView):
    template_name = 'author/author_update.html'
    form_class = AuthorForm
    model = Author
    success_url = '/admin_panel/reference/author/'

class DeleteAuthorViews(DeleteView):
    template_name= 'author/author_delete.html'
    form_class = AuthorForm
    model = Author
    success_url = '/admin_panel/reference/author/'



#Серия


class ListSerieView(ListView):
    template_name = 'serie/serie_page.html'
    model = Serie

class DetailSerieView(DetailView):
    template_name = 'serie/serie_detail_page.html'
    model = Serie

class CreateSerieView(CreateView):
    template_name = 'serie/serie_add.html'
    form_class = SerieForm
    success_url = '/admin_panel/reference/serie/'


class UpdateSerieView(UpdateView):
    template_name = 'serie/serie_update.html'
    form_class = SerieForm
    model = Serie
    success_url = '/admin_panel/reference/serie/'

class DeleteSerieViews(DeleteView):
    template_name= 'serie/serie_delete.html'
    form_class = SerieForm
    model = Serie
    success_url = '/admin_panel/reference/serie/'



#Жанры


class ListGenreView(ListView):
    template_name = 'genre/genre_page.html'
    model = Genre

class DetailGenreView(DetailView):
    template_name = 'genre/genre_detail_page.html'
    model = Genre

class CreateGenreView(CreateView):
    template_name = 'genre/genre_add.html'
    form_class = GenreForm
    success_url = '/admin_panel/reference/ganre/'


class UpdateGenreView(UpdateView):
    template_name = 'genre/genre_update.html'
    form_class = GenreForm
    model = Genre
    success_url = '/admin_panel/reference/ganre/'

class DeleteGenreViews(DeleteView):
    template_name= 'genre/genre_delete.html'
    form_class = GenreForm
    model = Genre
    success_url = '/admin_panel/reference/ganre/'

#Издательства

class ListPublishingHouseView(ListView):
    template_name = 'publishinghouse/publishinghouse_page.html'
    model = PublishingHouse

class DetailPublishingHouseView(DetailView):
    template_name = 'publishinghouse/publishinghouse_detail_page.html'
    model = PublishingHouse

class CreatePublishingHouseView(CreateView):
    template_name = 'publishinghouse/publishinghouse_add.html'
    form_class = PublishingHouseForm
    success_url = '/admin_panel/reference/publishinghouse/'


class UpdatePublishingHouseView(UpdateView):
    template_name = 'publishinghouse/publishinghouse_update.html'
    form_class = PublishingHouseForm
    model = PublishingHouse
    success_url = '/admin_panel/reference/publishinghouse/'

class DeletePublishingHouseViews(DeleteView):
    template_name= 'publishinghouse/publishinghouse_delete.html'
    form_class = PublishingHouseForm
    model = PublishingHouse
    success_url = '/admin_panel/reference/publishinghouse/'

#Изготовитель

class ListPrintingHouseView(ListView):
    template_name = 'pritinghouse/pritinghouse_page.html'
    model = PrintingHouse

class DetailPrintingHouseView(DetailView):
    template_name = 'pritinghouse/pritinghouse_detail_page.html'
    model = PrintingHouse

class CreatePrintingHouseView(CreateView):
    template_name = 'pritinghouse/pritinghouse_add.html'
    form_class = PrintingHouseForm
    success_url = '/admin_panel/reference/pritinghouse/'

class UpdatePrintingHouseView(UpdateView):
    template_name = 'pritinghouse/pritinghouse_update.html'
    form_class = PrintingHouseForm
    model = PrintingHouse
    success_url = '/admin_panel/reference/pritinghouse/'
class DeletePrintingHouseViews(DeleteView):
    template_name = 'pritinghouse/pritinghouse_delete.html'
    form_class = PrintingHouseForm
    model = PrintingHouse
    success_url = '/admin_panel/reference/pritinghouse/'