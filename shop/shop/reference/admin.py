from django.contrib import admin

# Register your models here.
from .models import Author, Serie, Genre, PublishingHouse, PrintingHouse

admin.site.register(Author)
admin.site.register(Serie)
admin.site.register(Genre)
admin.site.register(PublishingHouse)
admin.site.register(PrintingHouse)