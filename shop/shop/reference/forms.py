from django import forms


from .models import Author,Genre,Serie,PublishingHouse,PrintingHouse
#Авторы
class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author

        fields = [
            'name',
            'description'
        ]

#Серия
class SerieForm(forms.ModelForm):
    class Meta:
        model=Serie

        fields = [
            'name',
            'description'
        ]

# Жанры

class GenreForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = [
              'name',
             'description'
         ]
#Издательство

class PublishingHouseForm(forms.ModelForm):
    class Meta:
        model = PublishingHouse
        fields = [
        'name',
        'description'
        ]

#Изготовитель
class PrintingHouseForm(forms.ModelForm):
    class Meta:
        model = PrintingHouse
        fields = [
                'name',
                'description'
            ]

