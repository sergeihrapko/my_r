from django.urls import path
from . import  views
from .views import CreateAuthorView, UpdateAuthorView, DeleteAuthorViews,SpravkaView,ListAuthorView,DetailAuthorView,\
    CreateGenreView, UpdateGenreView, DeleteGenreViews,ListGenreView,DetailGenreView,\
    CreateSerieView, UpdateSerieView, DeleteSerieViews,ListSerieView,DetailSerieView, \
    CreatePublishingHouseView, UpdatePublishingHouseView, DeletePublishingHouseViews, ListPublishingHouseView, DetailPublishingHouseView,\
    CreatePrintingHouseView, UpdatePrintingHouseView, DeletePrintingHouseViews, ListPrintingHouseView, DetailPrintingHouseView


urlpatterns = [
    #для авторов
    path('',SpravkaView.as_view(),name='spravka'),
    path('create/author/',CreateAuthorView.as_view(),name='create_author'),
    path('update/author/<int:pk>/',UpdateAuthorView.as_view(),name='update_author'),
    path('delete/author/<int:pk>/',DeleteAuthorViews.as_view(),name='delete_author'),
    path('author/', ListAuthorView.as_view(), name='list_author'),
    path('detail/author/<int:pk>/',DetailAuthorView.as_view(),name='detail_author'),
#для серии
    path('create/serie/', CreateSerieView.as_view(), name='create_serie'),
    path('update/serie/<int:pk>/', UpdateSerieView.as_view(), name='update_serie'),
    path('delete/serie/<int:pk>/', DeleteSerieViews.as_view(), name='delete_serie'),
    path('serie/', ListSerieView.as_view(), name='list_serie'),
    path('detail/serie/<int:pk>/', DetailSerieView.as_view(), name='detail_serie'),

# для жанров
    path('create/ganre/', CreateGenreView.as_view(), name='create_ganre'),
    path('update/ganre/<int:pk>/', UpdateGenreView.as_view(), name='update_ganre'),
    path('delete/ganre/<int:pk>/', DeleteGenreViews.as_view(), name='delete_ganre'),
    path('ganre/', ListGenreView.as_view(), name='list_ganre'),
    path('detail/ganre/<int:pk>/', DetailGenreView.as_view(), name='detail_ganre'),
#для издательств
    path('create/publishinghouse/', CreatePublishingHouseView.as_view(), name='create_publishinghouse'),
    path('update/publishinghouse/<int:pk>/', UpdatePublishingHouseView.as_view(), name='update_publishinghouse'),
    path('delete/publishinghouse/<int:pk>/', DeletePublishingHouseViews.as_view(), name='delete_publishinghouse'),
    path('publishinghouse/', ListPublishingHouseView.as_view(), name='list_publishinghouse'),
    path('detail/publishinghouse/<int:pk>/', DetailPublishingHouseView.as_view(), name='detail_publishinghouse'),
#для изготовителей
    path('create/pritinghouse/', CreatePrintingHouseView.as_view(), name='create_pritinghouse'),
    path('update/pritinghouse/<int:pk>/', UpdatePrintingHouseView.as_view(), name='update_pritinghouse'),
    path('delete/pritinghouse/<int:pk>/', DeletePrintingHouseViews.as_view(), name='delete_pritinghouse'),
    path('pritinghouse/', ListPrintingHouseView.as_view(), name='list_pritinghouse'),
    path('detail/pritinghouse/<int:pk>/', DetailPrintingHouseView.as_view(), name='detail_pritinghouse'),

]