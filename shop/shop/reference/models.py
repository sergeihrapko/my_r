from django.db import models


class Author(models.Model):
    name = models.CharField(
        "Имя автора",
        max_length=200)
    description = models.TextField(
        "Описание",
        max_length=200)

    def __str__(self):
        return self.name


class Serie(models.Model):
    name = models.CharField(
        "Серия книг",
        max_length=200)
    description = models.CharField(
        "Описание",
        max_length=200)

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(
        "Жанр",
        max_length=200)
    description = models.CharField(
        "Описание",
        max_length=200)

    def __str__(self):
        return self.name


class PublishingHouse(models.Model):
    name = models.CharField(
        "Издательство",
        max_length=200)
    description = models.CharField(
        "Описание",
        max_length=200)

    def __str__(self):
        return self.name


class PrintingHouse(models.Model):
    name = models.CharField(
        "Изготовитель",
        max_length=200)
    description = models.CharField(
        "Описание",
        max_length=200)

    def __str__(self):
        return self.name

