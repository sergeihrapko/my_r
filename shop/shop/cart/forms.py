from django import forms

from .models import ProductsInCart


class AddProductForm(forms.ModelForm):
    class Meta:
        model = ProductsInCart

        fields = [
            'product',
            'quantity'
        ]
        widgets = {
            'product': forms.HiddenInput(),
        }
