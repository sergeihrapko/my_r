from django.db import models
from django.contrib.auth import get_user_model
from products.models import Book

User = get_user_model()


class Cart(models.Model):
    user = models.ForeignKey(
        User,
        verbose_name="Покупатель",
        blank=True,
        null=True,
        on_delete=models.CASCADE)
    created_date = models.DateTimeField(
        "Дата создания",
        auto_now=False,
        auto_now_add=True)
    updated_date = models.DateTimeField(
        "Дата изменения",
        auto_now=True,
        auto_now_add=False)

    def __str__(self):
        return "Корзина {}, покупателя {}".format(
            self.pk,
            self.user)

    @property
    def products_in_cart_count(self):
        total = 0
        for product in self.products_in_cart.all():
            total += product.quantity
        return total

    @property
    def total_price(self):
        total = 0
        for product in self.products_in_cart.all():
            total += product.price_total
        return total

    class Meta:
        verbose_name = "Корзина"
        verbose_name_plural = "Корзины"


class ProductsInCart(models.Model):
    cart = models.ForeignKey(
        Cart,
        verbose_name="Продукт в корзине",
        related_name="products_in_cart",
        on_delete=models.CASCADE)
    product = models.ForeignKey(
        Book,
        on_delete=models.CASCADE)
    quantity = models.IntegerField(
        "Количество")
    created_date = models.DateTimeField(
        "Дата создания",
        auto_now=False,
        auto_now_add=True)
    updated_date = models.DateTimeField(
        "Дата изменения",
        auto_now=True,
        auto_now_add=False)

    def __str__(self):
        return "Товар {name}, кол-во {quantity} в корзине".format(
            name=self.product.name,
            quantity=self.quantity)

    @property
    def price(self):
        return self.product.price

    @property
    def price_total(self):
        return self.product.price * self.quantity


    class Meta:
        verbose_name = "Товар в корзине"
        verbose_name_plural = "Товары в корзине"
        unique_together = (
            ('cart', 'product')
        )
